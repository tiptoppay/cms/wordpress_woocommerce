<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

$current_user_id     = get_current_user_id();
$tokens              = WC_Payment_Tokens::get_customer_tokens( $current_user_id, 'wc_tiptoppay_gateway' );
$wrapper_class       = $tokens ? ' tiptoppay--has-tokens' : ' tiptoppay--no-tokens';
$hide_save_cart      = $tokens ? ' hide-save_cart' : '';
$hide_widget_control = ! $tokens ? ' hide-widget_control' : '';
?>
<div class="tiptoppay<?php echo esc_attr( $wrapper_class ); ?>">

	<?php
	if ( $tokens ) {
		echo apply_filters( // phpcs:ignore
			'tiptoppay_checkout_token_choose_title_html',
			sprintf(
				'<p class="tiptoppay__choose-title">%s</p>',
				esc_html__( 'Выберите карту для оплаты или оплатите новой:', 'tiptoppay' )
			),
			$tokens
		);

		foreach ( $tokens as $key => $token ) {
			echo apply_filters( // phpcs:ignore
				'tiptoppay_checkout_token_control_html',
				sprintf(
					'<label class="tiptoppay-token-control"><input type="radio" onclick="saveCartBox();" name="ttp_card" id="ttp_card%s" value="%s" %s>%s</label>',
					esc_attr( $key ),
					esc_attr( $token->get_id() ),
					checked( 1, $token->is_default(), false ),
					esc_html( $token->get_card_type() . ' ************' . $token->get_last4() )
				),
				$key,
				$token,
				$tokens
			);
		}
	} else {
		echo apply_filters( // phpcs:ignore
			'tiptoppay_checkout_no_tokens_text',
			'<p>' . esc_html__( 'Для оплаты заказа вы будете перенаправлены на страницу сервиса TipTopPay.', 'tiptoppay' ) . '</p>',
			$tokens
		);
	}
	?>

	<label class="tiptoppay-pay-other-card<?php echo esc_attr( $hide_widget_control ); ?>">
		<input
			type="radio"
			name="ttp_card"
			id="ttp_card_checkout"
			onclick="saveCartBox();"
			value="widget"
			<?php checked( ! $tokens, true, true ); ?>>

		<?php
		echo apply_filters( // phpcs:ignore
			'tiptoppay_checkout_pay_other_card_label',
			esc_html__( 'Оплатить другой картой', 'tiptoppay' ),
			$tokens
		);
		?>
	</label>

	<?php if ( is_user_logged_in() ) : ?>
		<label class="tiptoppay-save-card ttp_save_card<?php echo esc_attr( $hide_save_cart ); ?>">
			<input
				type="checkbox"
				name="ttp_save_card"
				id="ttp_save_card"
				value="1"
				<?php checked( ! $tokens, true, true ); ?>>

			<?php
			echo apply_filters( // phpcs:ignore
				'tiptoppay_checkout_save_card_label',
				esc_html__( 'Сохранить как основную карту', 'tiptoppay' ),
				$tokens
			);
			?>
		</label>
	<?php endif; ?>

	<div id="paymentForm" style="display: none;">
		<input
			type="hidden"
			id="CardCryptogramPacket"
			name="CardCryptogramPacket">
		<input
			type="hidden"
			id="user"
			name="user"
			value="<?php echo esc_attr( $current_user_id ); ?>">
	</div>
</div>