<?php
/**
 * Plugin Name: TipTopPay Gateway for WooCommerce
 * Plugin URI: https://tiptoppay.kz/
 * Description: Extends WooCommerce with TipTopPay Gateway.
 * Version: 1.0.1
 * Author: TipTop Pay
 * Author URI: https://tiptoppay.kz/
 * License: GPL v2 or later
 * License URI: https://www.gnu.org/licenses/gpl-2.0.html
 */
if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

define('TTPGWWC_PLUGIN_URL', plugin_dir_url(__FILE__));
define('TTPGWWC_PLUGIN_DIR', plugin_dir_path(__FILE__));
define('TTPGWWC_PLUGIN_FILENAME', __FILE__);
 
require(TTPGWWC_PLUGIN_DIR . 'inc/class-tiptoppay-init.php');

if (class_exists('TipTopPay_Init')) {
    TipTopPay_Init::init();
}