jQuery(document).ready(function ($) {


    window.saveCartBox = () => {
        if ($('#ttp_card_checkout').is(":checked")) {
            $('.ttp_save_card').removeClass('hide-save_cart');
            $('#ttp_save_card').prop("checked", true);
        } else {
            $('.ttp_save_card').addClass('hide-save_cart');
            $('#ttp_save_card').prop("checked", false);
        }
    }

});