=== TipTopPay Gateway for WooCommerce ===
Author URI: https://tiptoppay.kz/
Contributors: tiptoppay
Tags: woocommerce, payment, payments, tiptoppay
Requires at least: 4.9.7
Tested up to: 6.7.1
Stable tag: 1.0.1
Requires PHP: 7.4
License: GPLv2 or later
License URI: https://www.gnu.org/licenses/gpl-2.0.html

Это официальный модуль TipTopPay, который позволит добавить на ваш магазин woocommerce оплату банковскими картами через платежный сервис [TipTopPay](https://tiptoppay.kz/). Модуль вызывает виджет с адреса https://widget.tiptoppay.kz/bundles/tiptoppay для ввода карточных данных в момент оплаты сформированного заказа. TipTopPay — сертифицированный сервис провайдер с максимальным уровнем соответствия PCI DSS. Подтверждение соответствия проходит каждый год в рамках сертификационного аудита.

== Description ==

## Возможности:
* Одностадийная схема оплаты;
* Двухстадийная схема;
* Оплата в 1 клик;
* Информирование о статусе платежа;
* Выбор языка виджета;
* Выбор дизайна виджета;
* Выбор валюты виджета
* Выбор способа доставки для платежа
* Возможность указать назначение платежа;
* Поддержка WooCommerce Subscriptions;
* Поддержка онлайн-касс (ФЗ-54);
* Отправка чеков по email;
* Отправка чеков по SMS;
* Теги способа и предмета расчета; 
* Отдельный параметр НДС для доставки.

## Совместимость:

WordPress 4.9.7 и выше;
WooCommerce 3.4.4 и выше.

== Installation ==

## Установка

1. Скопируйте папку `tiptoppay_gateway_for_woocommerce` в директорию `wp-content/plugins/` на вашем сервере или установите плагин напрямую через раздел плагинов WordPress.

2. Зайдите в "Управление сайтом" -> "Плагины". Активируйте плагин "TipTopPay Gateway for WooCommerce".

3. В управлении сайтом зайдите в "WooCommerce" -> "Настройки" -> "Оплата" -> "TipTopPay". Отметьте галочкой  "Enable TipTopPay".

* **Включить/Выключить** - Включение/Отключение платежной системы;  
* **Включить DMS** - Включение двухстадийной схемы оплата платежа (холдирование);
* **Текст оплаты заказа** - Можете настроить текст назначения платежа;  
* **Статус для оплаченного заказа** - **Обработка** (Если не предусматривается другой функционал);  
* **Статус для отмененного заказа** - **Отменен** (Если не предусматривается другой функционал);  
* **Статус авторизованного платежа DMS** - **На удержании** (Или **Платеж авторизован**, если предусматривается другой функционал);
* **Наименование** - Заголовок, который видит пользователь в процессе оформления заказа;  
* **Описание** - Описание метода оплаты;  
* **Public_id** - Public id сайта из личного кабинета TipTopPay;
* **Password for API** - API Secret из личного кабинета TipTopPay;
* **Валюта магазина** - Можно выбрать либо валюту магазина, либо конкретное значение, которое будет передаваться в платеж;  
* **Дизайн виджета** - Выбор дизайна виджета из 3 возможных (classic, modern, mini);
* **Язык виджета** - Русский МСК (Если не предусматривается использовать другие языки); 
* **Включено для способов доставки** - Выбор способа доставки, для которого возможен платеж**.

Использовать функционал онлайн касс:
* **Включить/выключить** - Включение/отключение формирования онлайн-чека при оплате;
* **ИИН/БИН** - ИИН/БИН вашей организации или ИП;
* **Ставка НДС** - Укажите ставку НДС товаров;
* **Ставка НДС для доставки** - Укажите ставку НДС службы доставки;
* **Система налогообложения организации** - Тип системы налогообложения;
* **Способ расчета** - признак способа расчета;
* **Предмет расчета** - признак предмета расчета;
* **Статус которым пробивать 2ой чек при отгрузке товара или выполнении услуги** - **Выполнен** (Или **Доставлен**, если предусматривается другой функционал);
* **Действие со штрих-кодом** - отправление артикула товара в чек как штрих-код.
_Согласно ФЗ-54 владельцы онлайн-касс должны формировать чеки для зачета и предоплаты. Отправка второго чека возможна только при следующих способах расчета: Предоплата, Предоплата 100%, Аванс._  

Нажмите "Сохранить изменения".

В личном кабинете TipTopPay зайдите в настройки сайта, пропишите в настройках уведомления, как описано на странице настройки модуля на указанный адрес:

Вы готовы принимать платежи с банковских карт с помощью TipTopPay!

== Frequently Asked Questions ==

= Какой порядок подключения? =

Для подключения к системе приема платежей TipTopPay, необходимо выполнить следующие действия:
* Оставить заявку.
* Получить ответ от персонального менеджера. Он будет сопровождать на всех этапах.
* Ознакомиться с преимуществами работы и списком необходимых документов.
* Получить доступ в личный кабинет. Он необходим для подписания договора и дальнейшей работы со всеми инструментариями TipTopPay.
* Выполнить техническую интеграцию сайта.
* Провести тестовые платежи. После успешных тестов — сообщить об этом менеджеру, который переведет сайт в боевой режим. 
* Принимайте онлайн-платежи на сайте с помощью банковских карт, а также в один клик
 
= Как получить URL адрес с копией отправленного онлайн-чека в админ панели магазина? =

Для получения URL адреса копии отправленного онлайн-чека в комментариях к заказу необходимо прописать в личном кабинете TipTopPay адрес для уведомления. Для этого зайдите в настройки сайта, пропишите в настройках адрес:

* **Receipt Уведомление** (Уведомление об онлайн-чеке):\
http://domain.kz/wordpress/wc-api/wc_tiptoppay_gateway?action=receipt

Где domain.kz — доменное имя вашего сайта.

Для некоторых товаров есть требование поставлять названия товаров в чеках на русском языке. Для этого добавлены несколько фильтров:

* Формирование данных заказа во время оплаты  
    * tiptoppay_process_payment_shipping_data
    * tiptoppay_process_payment_order_item
* Формирование данных заказа на при смене статуса заказа  
    * tiptoppay_send_receipt_item
    * tiptoppay_send_receipt_shipping_data
    * tiptoppay_send_receipt_data
* Формирование данных заказа при запланированном платеже  
    * tiptoppay_scheduled_subscription_payment_shipping_data
    * tiptoppay_scheduled_subscription_payment_order_item

== 3rd party services ==

Обработка платежей происходит c использованием [интернет-эквайринга TipTop Pay Kazakhstan](https://tiptoppay.kz/online-store),
в частности используется [платежный виджет](https://widget.tiptoppay.kz/bundles/widget.js), а также [API](https://api.tiptoppay.kz/) для формирования чеков и проведения операций с платежами

При использовании плагина вы соглашаетесь с [офертой TipTop Pay Kazakhstan](https://static.tiptoppay.kz/docs/offer_rus.pdf)

[ПРАВИЛА ОСУЩЕСТВЛЕНИЯ ДЕЙЯТЕЛЬНОСТИ ПЛАТЕЖНОЙ ОРГАНИЗАЦИИ](https://static.tiptoppay.kz/docs/rules_rus.pdf)
[ПОЛИТИКА КОНФИДЕНЦИАЛЬНОСТИ](https://static.tiptoppay.kz/docs/privacy_policy.pdf)